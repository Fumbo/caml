open Ast

let print_params f l =
    let rec print_list_rec li = match li with
        | [] -> Printf.printf ")"
        | e::[] -> f e; Printf.printf ")"
        | e::l -> f e; Printf.printf ","; print_list_rec l
    in 
        Printf.printf "("; print_list_rec l

let rec print_expr = function
    | Int a -> Printf.printf "%i" a
    | Var s -> Printf.printf "%s" s
    | BinOp (a,s,b) -> print_expr a; Printf.printf " %s " s; print_expr b
    | UniOp (s,a) -> Printf.printf "%s" s; print_expr a
    | Call (s,l) -> Printf.printf "%s" s; print_params print_expr l

let rec print_statement_list f = function
    | [] -> ()
    | e::l -> f e; print_statement_list f l

let rec print_statement = function
    | Assign (s,expr) -> Printf.printf "%s = " s; print_expr expr;
                         print_endline ";"
    | Expr expr -> print_expr expr; print_endline ";"
    | If (expr,l1,[]) -> Printf.printf "if (";
                         print_expr expr; print_endline ")";
                         print_endline "{";
                         print_statement_list print_statement l1;
                         print_endline "}";
    | If (expr,l1,l2) -> Printf.printf "if (";
                         print_expr expr; print_endline ")";
                         print_endline "{";
                         print_statement_list print_statement l1;
                         print_endline "}";
                         print_endline "else";
                         print_endline "{";
                         print_statement_list print_statement l2;
                         print_endline "}"
    | While (expr,l) -> Printf.printf "while (";
                        print_expr expr; print_endline ")";
                        print_endline "{";
                        print_statement_list print_statement l;
                        print_endline "}"
    | Return expr -> Printf.printf "return "; print_expr expr; print_endline ";"

let rec print_fbody = function
    | [] -> ()
    | e::l -> print_statement e; print_fbody l

let print_fparams l =
    let rec print_list_rec li = match li with
         | [] -> print_endline ")"
        | e::[] -> Printf.printf "%s" e; print_endline ")"
        | e::l -> Printf.printf "%s," e; print_list_rec l
    in 
        Printf.printf "("; print_list_rec l

let print_fvars l =
    let rec print_list_rec li = match li with
        | [] -> print_endline ";"
        | e::[] -> Printf.printf "%s" e; print_endline ";"
        | e::l -> Printf.printf "%s," e; print_list_rec l
    in 
        Printf.printf "vars "; print_list_rec l

let rec print_func = function
    | [] -> ()
    | e::l -> Printf.printf "%s" e.fname;
              print_fparams e.fparams;
              print_endline "{";
              print_fvars e.fvars;
              print_fbody e.fbody;
              print_endline "}";
              print_newline ();
              print_func l

let print_main a =
    begin
        print_endline "main()";
        print_endline "{";
        print_fvars a.mainvars;
        print_fbody a.mainbody;
        print_endline "}";
    end

let printer a =
    begin
        print_func a.func;
        print_main a.main;
    end


let rec evalstat body hash =
    let rec evalexpr hash = function
    | Int a -> a
    | Var s -> Hashtbl.find hash s
    | BinOp (a,s,b) -> (match s with
       |"+" -> (evalexpr hash a) + (evalexpr hash b)
       |"-" -> (evalexpr hash a) - (evalexpr hash b)
       |"*" -> (evalexpr hash a) * (evalexpr hash b)
       |"/" -> (evalexpr hash a) / (evalexpr hash b)
       |">" -> if ((evalexpr hash a) > (evalexpr hash b)) then 1 else 0
       |"<" -> if ((evalexpr hash a) < (evalexpr hash b)) then 1 else 0
       |">=" -> if ((evalexpr hash a) >= (evalexpr hash b)) then 1 else 0
       |"<=" -> if ((evalexpr hash a) <= (evalexpr hash b)) then 1 else 0
       |"==" -> if ((evalexpr hash a) == (evalexpr hash b)) then 1 else 0
       |"||" -> (evalexpr hash a) + (evalexpr hash b)
       |"&&" -> (evalexpr hash a) * (evalexpr hash b)
       |"!" -> if ((evalexpr hash a) <> (evalexpr hash b)) then 1 else 0)
    | UniOp (s,a) -> (match s with
       |"!" -> if (evalexpr hash a) == 0 then 1 else 0
       |"-" -> (evalexpr hash a) * (-1)
       |"++" -> (evalexpr hash a) + 1
       |"--" -> (evalexpr hash a) - 1)
    | Call (s, el) -> (
        match s with
        |"write" ->
                if (List.length el == 0) then
                    1
                else
                    begin
                        for i=0 to (List.length el)-1 do
                            Printf.printf "%i" (evalexpr hash (List.nth el i));
                            print_newline ();
                        done;
                        1
                    end
        |_ -> 1) in
    let l2 = List.length body in
    for i = 0 to l2-1 do
        match (List.nth body i) with
        (* Evaluer une expression avec variable *)
        (* Evaluer un appel a "write" *)
        |Expr expr -> evalexpr hash expr
        (* Evaluer une affectation *)
        |Assign (s, expr) -> Hashtbl.replace hash s (evalexpr hash expr); 1
        |If (c, i, e) -> if ((evalexpr hash c) == 0) then evalstat e hash
        else evalstat i hash; 1
        |While (c, d) -> while ((evalexpr hash c) <> 0) do
            evalstat d hash
                        done;
                        1
    done

let eval ast = 
    (* Evaluation des Variables *)
    let l = List.length ast.main.mainvars in
    let l2 = List.length ast.func in
    let hash = Hashtbl.create l in
    for i=0 to l-1 do
        Hashtbl.add hash (List.nth ast.main.mainvars i) 0
    done;
    (* Evaluation du corps du Main *)
    let body = ast.main.mainbody in
    evalstat body hash


let main () =
    begin
        let i = ref 1 in
        if Array.length (Sys.argv) < 2 then
            failwith "Il manque le nom du fichier!";
        if Sys.argv.(1) = "-eval" then
                i := !i + 1;
        let cin = open_in Sys.argv.(!i) in
        let ast = Parser.prg Lexer.token (Lexing.from_channel cin) in
        if Sys.argv.(1) = "-eval" then
                eval ast;
        if Array.length (Sys.argv) = 2 then
            printer ast;
        exit 0;
            end

let _ = main ()
